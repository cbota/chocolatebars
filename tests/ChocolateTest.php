<?php

declare(strict_types=1);

namespace App\Test;

use App\Chocolate;
use PHPUnit\Framework\TestCase;

class ChocolateTest extends TestCase
{
    /**
     * @var Chocolate
     */
    public $chocolate;

    /**
     * {@inheritDoc}
     */
    public function setUp(): void
    {
        $this->chocolate = new Chocolate();
    }

    /**
     * @dataProvider getChocolates()
     *
     * @param int|string $numberOfChocolates
     * @param int|string $expected
     */
    public function testCapitalize($numberOfChocolates, $expected)
    {
        $capitalized = $this->chocolate->capitalize($numberOfChocolates);
        $this->assertSame($expected, $capitalized);
    }

    /**
     * @return array
     */
    public function getChocolates()
    {
      return [
          [48, 48],
          [3, 3],
          [5, 5],
          ["one", "One"],
          ["no more", "No more"]
      ];
    }

    /**
     * @dataProvider getNumberOfChocolates()
     *
     * @param int $numberOfChocolates
     * @param $expected
     */
    public function testNumberOfChocolates(int $numberOfChocolates, $expected)
    {
        $chocolates = $this->chocolate->numberOfChocolates($numberOfChocolates);

        $this->assertSame($expected, $chocolates);
    }

    /**
     * @return array
     */
    public function getNumberOfChocolates()
    {
        return [
            [48, 48],
            [2, 2],
            [1, "one"],
            [0, "no more"]
        ];
    }

    /**
     * @dataProvider getNumberOfChocolatesAfterTakeout()
     *
     * @param int $numberOfChocolates
     * @param $expected
     */
    public function testNumberOfChocolatesAfterTakeout(int $numberOfChocolates, $expected)
    {
        $chocolates = $this->chocolate->numberOfChocolatesAfterTakeout($numberOfChocolates);

        $this->assertSame($expected, $chocolates);
    }

    /**
     * @return array
     */
    public function getNumberOfChocolatesAfterTakeout()
    {
        return [
            [48, 48],
            [2, 2],
            [0, "no more"],
            [-2, 48]
        ];
    }

    /**
     * @dataProvider getQuantity()
     *
     * @param int $numberOfChocolates
     * @param $expected
     */
    public function testQuantity(int $numberOfChocolates, $expected)
    {
        $chocolates = $this->chocolate->quantity($numberOfChocolates);

        $this->assertSame($expected, $chocolates);
    }

    /**
     * @return array
     */
    public function getQuantity()
    {
        return [
            [48, " pieces of"],
            [2, " pieces of"],
            [1, ""],
            [0, ""]
        ];
    }

    /**
     * @dataProvider getContainer()
     *
     * @param int $numberOfChocolates
     * @param $expected
     */
    public function testContainer(int $numberOfChocolates, $expected)
    {
        $chocolates = $this->chocolate->container($numberOfChocolates);

        $this->assertSame($expected, $chocolates);
    }

    /**
     * @return array
     */
    public function getContainer()
    {
        return [
            [48, "bars"],
            [2, "bars"],
            [1, "bar"],
            [0, "bar"]
        ];
    }

    /**
     * @dataProvider getAction()
     *
     * @param int $numberOfChocolates
     * @param $expected
     */
    public function testAction(int $numberOfChocolates, $expected)
    {
        $chocolates = $this->chocolate->action($numberOfChocolates);

        $this->assertSame($expected, $chocolates);
    }

    /**
     * @return array
     */
    public function getAction()
    {
        return [
            [48, "Take one out of the box, pass it around,"],
            [2, "Take one out of the box, pass it around,"],
            [1, "Take one out of the box, pass it around,"],
            [0, "Go to the store and buy another box,"]
        ];
    }
}
