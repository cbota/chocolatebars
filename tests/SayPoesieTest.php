<?php

declare(strict_types=1);

namespace App\Test;

use App\Poesy;
use App\Chocolate;
use PHPUnit\Framework\TestCase;

class SayPoesieTest extends TestCase
{
    /**
     * @dataProvider getChocolatePoesy
     *
     * @param int $chocolateNum
     * @param string $expected
     */
    public function testSayPoesie(int $chocolateNum, string $expected)
    {
        $chocolate = new Chocolate($chocolateNum);
        $poesy = new Poesy($chocolate);
        $result = $poesy->sayPoesy($chocolateNum);

        $this->assertSame($expected, $result);
    }

    /**
     * @return array
     */
    public function getChocolatePoesy()
    {
        return [
            [
                3,
                "3 pieces of chocolate bars in the box, 3 chocolate bars.\n" .
                "Take one out of the box, pass it around, 2 chocolate bars in the box.\n\n\n" .
                "2 pieces of chocolate bars in the box, 2 chocolate bars.\n" .
                "Take one out of the box, pass it around, 1 chocolate bar in the box.\n\n\n" .
                "One chocolate bar in the box, one chocolate bar.\n" .
                "Take one out of the box, pass it around, no more chocolate bar in the box.\n\n\n" .
                "No more chocolate bar in the box, no more chocolate bar.\n" .
                "Go to the store and buy another box, 3 chocolate bars in the box."
            ],
            [
                1,
                "One chocolate bar in the box, one chocolate bar.\n" .
                "Take one out of the box, pass it around, no more chocolate bar in the box.\n\n\n" .
                "No more chocolate bar in the box, no more chocolate bar.\n" .
                "Go to the store and buy another box, 1 chocolate bars in the box."
            ],
            [
                48,
                "48 pieces of chocolate bars in the box, 48 chocolate bars.\n" .
                "Take one out of the box, pass it around, 47 chocolate bars in the box.\n\n\n" .
                "47 pieces of chocolate bars in the box, 47 chocolate bars.\n" .
                "Take one out of the box, pass it around, 46 chocolate bars in the box.\n\n\n" .
                "46 pieces of chocolate bars in the box, 46 chocolate bars.\n" .
                "Take one out of the box, pass it around, 45 chocolate bars in the box.\n\n\n" .
                "45 pieces of chocolate bars in the box, 45 chocolate bars.\n" .
                "Take one out of the box, pass it around, 44 chocolate bars in the box.\n\n\n" .
                "44 pieces of chocolate bars in the box, 44 chocolate bars.\n" .
                "Take one out of the box, pass it around, 43 chocolate bars in the box.\n\n\n" .
                "43 pieces of chocolate bars in the box, 43 chocolate bars.\n" .
                "Take one out of the box, pass it around, 42 chocolate bars in the box.\n\n\n" .
                "42 pieces of chocolate bars in the box, 42 chocolate bars.\n" .
                "Take one out of the box, pass it around, 41 chocolate bars in the box.\n\n\n" .
                "41 pieces of chocolate bars in the box, 41 chocolate bars.\n" .
                "Take one out of the box, pass it around, 40 chocolate bars in the box.\n\n\n" .
                "40 pieces of chocolate bars in the box, 40 chocolate bars.\n" .
                "Take one out of the box, pass it around, 39 chocolate bars in the box.\n\n\n" .
                "39 pieces of chocolate bars in the box, 39 chocolate bars.\n" .
                "Take one out of the box, pass it around, 38 chocolate bars in the box.\n\n\n" .
                "38 pieces of chocolate bars in the box, 38 chocolate bars.\n" .
                "Take one out of the box, pass it around, 37 chocolate bars in the box.\n\n\n" .
                "37 pieces of chocolate bars in the box, 37 chocolate bars.\n" .
                "Take one out of the box, pass it around, 36 chocolate bars in the box.\n\n\n" .
                "36 pieces of chocolate bars in the box, 36 chocolate bars.\n" .
                "Take one out of the box, pass it around, 35 chocolate bars in the box.\n\n\n" .
                "35 pieces of chocolate bars in the box, 35 chocolate bars.\n" .
                "Take one out of the box, pass it around, 34 chocolate bars in the box.\n\n\n" .
                "34 pieces of chocolate bars in the box, 34 chocolate bars.\n" .
                "Take one out of the box, pass it around, 33 chocolate bars in the box.\n\n\n" .
                "33 pieces of chocolate bars in the box, 33 chocolate bars.\n" .
                "Take one out of the box, pass it around, 32 chocolate bars in the box.\n\n\n" .
                "32 pieces of chocolate bars in the box, 32 chocolate bars.\n" .
                "Take one out of the box, pass it around, 31 chocolate bars in the box.\n\n\n" .
                "31 pieces of chocolate bars in the box, 31 chocolate bars.\n" .
                "Take one out of the box, pass it around, 30 chocolate bars in the box.\n\n\n" .
                "30 pieces of chocolate bars in the box, 30 chocolate bars.\n" .
                "Take one out of the box, pass it around, 29 chocolate bars in the box.\n\n\n" .
                "29 pieces of chocolate bars in the box, 29 chocolate bars.\n" .
                "Take one out of the box, pass it around, 28 chocolate bars in the box.\n\n\n" .
                "28 pieces of chocolate bars in the box, 28 chocolate bars.\n" .
                "Take one out of the box, pass it around, 27 chocolate bars in the box.\n\n\n" .
                "27 pieces of chocolate bars in the box, 27 chocolate bars.\n" .
                "Take one out of the box, pass it around, 26 chocolate bars in the box.\n\n\n" .
                "26 pieces of chocolate bars in the box, 26 chocolate bars.\n" .
                "Take one out of the box, pass it around, 25 chocolate bars in the box.\n\n\n" .
                "25 pieces of chocolate bars in the box, 25 chocolate bars.\n" .
                "Take one out of the box, pass it around, 24 chocolate bars in the box.\n\n\n" .
                "24 pieces of chocolate bars in the box, 24 chocolate bars.\n" .
                "Take one out of the box, pass it around, 23 chocolate bars in the box.\n\n\n" .
                "23 pieces of chocolate bars in the box, 23 chocolate bars.\n" .
                "Take one out of the box, pass it around, 22 chocolate bars in the box.\n\n\n" .
                "22 pieces of chocolate bars in the box, 22 chocolate bars.\n" .
                "Take one out of the box, pass it around, 21 chocolate bars in the box.\n\n\n" .
                "21 pieces of chocolate bars in the box, 21 chocolate bars.\n" .
                "Take one out of the box, pass it around, 20 chocolate bars in the box.\n\n\n" .
                "20 pieces of chocolate bars in the box, 20 chocolate bars.\n" .
                "Take one out of the box, pass it around, 19 chocolate bars in the box.\n\n\n" .
                "19 pieces of chocolate bars in the box, 19 chocolate bars.\n" .
                "Take one out of the box, pass it around, 18 chocolate bars in the box.\n\n\n" .
                "18 pieces of chocolate bars in the box, 18 chocolate bars.\n" .
                "Take one out of the box, pass it around, 17 chocolate bars in the box.\n\n\n" .
                "17 pieces of chocolate bars in the box, 17 chocolate bars.\n" .
                "Take one out of the box, pass it around, 16 chocolate bars in the box.\n\n\n" .
                "16 pieces of chocolate bars in the box, 16 chocolate bars.\n" .
                "Take one out of the box, pass it around, 15 chocolate bars in the box.\n\n\n" .
                "15 pieces of chocolate bars in the box, 15 chocolate bars.\n" .
                "Take one out of the box, pass it around, 14 chocolate bars in the box.\n\n\n" .
                "14 pieces of chocolate bars in the box, 14 chocolate bars.\n" .
                "Take one out of the box, pass it around, 13 chocolate bars in the box.\n\n\n" .
                "13 pieces of chocolate bars in the box, 13 chocolate bars.\n" .
                "Take one out of the box, pass it around, 12 chocolate bars in the box.\n\n\n" .
                "12 pieces of chocolate bars in the box, 12 chocolate bars.\n" .
                "Take one out of the box, pass it around, 11 chocolate bars in the box.\n\n\n" .
                "11 pieces of chocolate bars in the box, 11 chocolate bars.\n" .
                "Take one out of the box, pass it around, 10 chocolate bars in the box.\n\n\n" .
                "10 pieces of chocolate bars in the box, 10 chocolate bars.\n" .
                "Take one out of the box, pass it around, 9 chocolate bars in the box.\n\n\n" .
                "9 pieces of chocolate bars in the box, 9 chocolate bars.\n" .
                "Take one out of the box, pass it around, 8 chocolate bars in the box.\n\n\n" .
                "8 pieces of chocolate bars in the box, 8 chocolate bars.\n" .
                "Take one out of the box, pass it around, 7 chocolate bars in the box.\n\n\n" .
                "7 pieces of chocolate bars in the box, 7 chocolate bars.\n" .
                "Take one out of the box, pass it around, 6 chocolate bars in the box.\n\n\n" .
                "6 pieces of chocolate bars in the box, 6 chocolate bars.\n" .
                "Take one out of the box, pass it around, 5 chocolate bars in the box.\n\n\n" .
                "5 pieces of chocolate bars in the box, 5 chocolate bars.\n" .
                "Take one out of the box, pass it around, 4 chocolate bars in the box.\n\n\n" .
                "4 pieces of chocolate bars in the box, 4 chocolate bars.\n" .
                "Take one out of the box, pass it around, 3 chocolate bars in the box.\n\n\n" .
                "3 pieces of chocolate bars in the box, 3 chocolate bars.\n" .
                "Take one out of the box, pass it around, 2 chocolate bars in the box.\n\n\n" .
                "2 pieces of chocolate bars in the box, 2 chocolate bars.\n" .
                "Take one out of the box, pass it around, 1 chocolate bar in the box.\n\n\n" .
                "One chocolate bar in the box, one chocolate bar.\n" .
                "Take one out of the box, pass it around, no more chocolate bar in the box.\n\n\n" .
                "No more chocolate bar in the box, no more chocolate bar.\n" .
                "Go to the store and buy another box, 48 chocolate bars in the box."
            ]
        ];
    }
}
