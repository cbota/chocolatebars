<?php

declare(strict_types=1);

namespace App;

use App\Enum\PoesyEnum;

/**
 * Class Poesy
 * @package App
 */
class Poesy
{
    /**
     * @var Chocolate
     */
    private $chocolate;

    /**
     * @param Chocolate $chocolate
     */
    public function __construct(Chocolate $chocolate)
    {
        $this->chocolate = $chocolate;
    }

    /**
     * @param int $numberOfVerses
     *
     * @return string
     */
    public function sayPoesy(int $numberOfVerses = 48): string
    {
        $poesy = '';
        for ($i = $numberOfVerses; $i >= 0; $i--) {
            if ($i == 0) {
                $poesy .= self::generateLyrics($i);
            } else {
                $poesy .= self::generateLyrics($i) . PoesyEnum::MULTIPLE_EOL;

            }
        }
        return $poesy;
    }

    /**
     * @param int $numberOfChocolateBars
     *
     * @return string
     */
    public function generateLyrics(int $numberOfChocolateBars): string
    {
        return
            $this->chocolate->capitalize($this->chocolate->numberOfChocolates($numberOfChocolateBars)) .
            $this->chocolate->quantity($numberOfChocolateBars) . PoesyEnum::EMPTY_SPACE . "chocolate". PoesyEnum::EMPTY_SPACE .
            $this->chocolate->container($numberOfChocolateBars) . PoesyEnum::EMPTY_SPACE . "in the box," . PoesyEnum::EMPTY_SPACE .
            $this->chocolate->numberOfChocolates($numberOfChocolateBars) .
            PoesyEnum::EMPTY_SPACE . "chocolate" . PoesyEnum::EMPTY_SPACE .
            $this->chocolate->container($numberOfChocolateBars) . "." . PoesyEnum::EOL.
            $this->chocolate->action($numberOfChocolateBars) . PoesyEnum::EMPTY_SPACE .
            $this->chocolate->numberOfChocolatesAfterTakeout($numberOfChocolateBars - 1) .
            PoesyEnum::EMPTY_SPACE . "chocolate" . PoesyEnum::EMPTY_SPACE .
            $this->chocolate->container($numberOfChocolateBars - 1) . PoesyEnum::EMPTY_SPACE . "in the box.";
    }
}
