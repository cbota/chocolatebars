<?php

declare(strict_types=1);

namespace App;

use App\Enum\PoesyEnum;

class Chocolate
{
    /**
     * @var int
     */
    private $numberOfChocolateBars;

    /**
     * @param int $numberOfChocolateBars
     */
    public function __construct(int $numberOfChocolateBars = 48)
    {
        $this->numberOfChocolateBars = $numberOfChocolateBars;
    }

    /**
     * @param int|string $numberOfChocolates
     *
     * @return string
     */
    public function capitalize($numberOfChocolates)
    {
        if (is_string($numberOfChocolates)) {
            return ucfirst($numberOfChocolates);
        } else {
            return $numberOfChocolates;
        }
    }

    /**
     * @param int $chocolates
     *
     * @return int|string
     */
    public function numberOfChocolates(int $chocolates)
    {
        if ($chocolates == 1) {
            return "one";
        } elseif ($chocolates == 0) {
            return "no more";
        } else {
            return $chocolates;
        }
    }

    /**
     * @param int $chocolates
     *
     * @return int|string
     */
    public function numberOfChocolatesAfterTakeout(int $chocolates)
    {
        if ($chocolates == 0) {
            return "no more";
        } elseif ($chocolates < 0) {
            return $this->numberOfChocolateBars;
        } else {
            return $chocolates;
        }
    }

    /**
     * @param int $chocolates
     *
     * @return string
     */
    public function quantity(int $chocolates): string
    {
        if ($chocolates > 1) {
            return PoesyEnum::EMPTY_SPACE . "pieces of";
        }

        return "";
    }

    /**
     * @param int $chocolates
     *
     * @return string
     */
    public function container(int $chocolates): string
    {
        if ($chocolates == 1 || $chocolates == 0) {
            return "bar";
        } else {
            return "bars";
        }
    }

    /**
     * @param int $chocolates
     *
     * @return string
     */
    public function action(int $chocolates): string
    {
        if ($chocolates == 0) {
            return "Go to the store and buy another box,";
        } else {
            return "Take one out of the box, pass it around,";
        }
    }
}
