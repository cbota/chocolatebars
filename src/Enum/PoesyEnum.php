<?php

declare(strict_types=1);

namespace App\Enum;

class PoesyEnum
{
    public const DEFAULT = 48;

    public const EMPTY_SPACE = " ";
    public const EOL = "\n";
    public const MULTIPLE_EOL = "\n\n\n";
}
