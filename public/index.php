<?php

declare(strict_types=1);

namespace App;

use App\Enum\PoesyEnum;

require __DIR__ . '/../vendor/autoload.php';

if(count($argv) > 2) {
    echo "Too much arguments. Use just one!\n";
    die;
}

$generateLyrics = new Chocolate((!empty($argv[1]) ? (int) $argv[1] : PoesyEnum::DEFAULT));
$chocolates = new Poesy($generateLyrics);

if (!empty($argv[1])) {
    echo $chocolates->sayPoesy((int)$argv[1]);
} else {
    echo $chocolates->sayPoesy(PoesyEnum::DEFAULT);
}
